
/* Game namespace */
var game = {
    // Run on page load.
    "onload" : function () {
        // Initialize the video.
        if (!me.video.init("screen", 768, 480, true, 'auto')) {
            alert("Your browser does not support HTML5 canvas.");
            return;
        }
		
		// add "#debug" to the URL to enable the debug Panel
		if (document.location.hash === "#debug") {
			window.onReady(function () {
				me.plugin.register.defer(debugPanel, "debug");
			});
		}

        // Initialize the audio.
        me.audio.init("mp3,ogg");

        // Set a callback to run when loading is complete.
        me.loader.onload = this.loaded.bind(this);
     
        // Load the resources.
        me.loader.preload(game.resources);

        // Initialize melonJS and display a loading screen.
        me.state.change(me.state.LOADING);
        
    },



    // Run on game resources loaded.
    "loaded" : function () {
//        me.state.set(me.state.MENU, new game.TitleScreen());
		me.entityPool.add("player", PlayerEntity);
		me.entityPool.add("projectile", ProjectileEntity);
		me.entityPool.add("turret", TurretEntity);
		me.entityPool.add("powerup", PowerupEntity);
		me.entityPool.add("health", HealthEntity);
		me.entityPool.add("demon", DemonEntity);
		me.entityPool.add("shield", ShieldEntity);
		me.entityPool.add("game_end", GameEndEntity);
    	
		me.input.bindKey(me.input.KEY.UP,   "up");
		me.input.bindKey(me.input.KEY.DOWN, "down");
		me.input.bindKey(me.input.KEY.ENTER, "go");
		me.input.bindKey(me.input.KEY.ESCAPE, "back");
    	
		// enable the keyboard
		me.input.bindKey(me.input.KEY.LEFT,  "left");
		me.input.bindKey(me.input.KEY.RIGHT, "right");
		me.input.bindKey(me.input.KEY.UP,    "jump", true);
		me.input.bindKey(me.input.KEY.Z,     "wswitch", true);
		me.input.bindKey(me.input.KEY.X,     "gswitch", true);
		me.input.bindKey(me.input.KEY.C,     "fswitch", true);
    	
		me.input.bindKey(me.input.KEY.A,       "left2");
		me.input.bindKey(me.input.KEY.D,       "right2");
		me.input.bindKey(me.input.KEY.W,       "jump2", true);
		me.input.bindKey(me.input.KEY.J,   "wswitch2", true);
		me.input.bindKey(me.input.KEY.K,  "gswitch2", true);
		me.input.bindKey(me.input.KEY.L,   "fswitch2", true);

		me.state.transition("fade", "#000000", 250);
        me.state.set(me.state.PLAY, new game.PlayScreen());
        me.state.set(me.state.TITLE, new game.TitleScreen());
        me.state.set(me.state.CREDITS, new game.CreditsScreen());
        me.state.set(me.state.GAME_END, new game.GameEndScreen());

        // Start the game.
        me.state.change(me.state.TITLE);
    }
};
