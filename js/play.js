game.PlayScreen = me.ScreenObject.extend(
	{
		/**	
		 *  action to perform on state change
		 */
		onResetEvent: function() {	
			
			me.gamestat.add("fEnabled", 0);
			me.gamestat.add("wEnabled", 0);
			me.gamestat.add("health", 0);
			
			me.gamestat.setValue("health", 5);

			me.levelDirector.loadLevel("level_1");
			
			me.game.addHUD(5,5, me.video.getWidth()-10, 20);
			me.game.HUD.addItem("score", new game.ScoreObject(0, 0));
	//
	//		me.game.add(me.entityPool.newInstanceOf("player"));
	//		me.game.collisionMap = me.game.currentLevel.getLayerByName("collision_g");
	//		me.game.currentLevel.getLayerByName("f_f").setOpacity(.5);
	//		me.game.currentLevel.getLayerByName("f_w").setOpacity(.5);

	},
	
	
	/**	
	 *  action to perform when leaving this screen (state change)
	 */
	onDestroyEvent: function() {
		me.game.disableHUD();
		me.game.removeAll();
	}
});
