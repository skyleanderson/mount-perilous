game.resources = [

	/* Graphics. 
	 */
	//game
	 {name: "tilemap",        type:"image", src: "res/img/tilemap.png"},
	 {name: "metatiles16x16", type:"image", src: "res/img/metatiles16x16.png"},
	 {name: "player_walk",    type:"image", src: "res/img/treasure-hunter-sheet.png"},
	 {name: "f_turret",       type:"image", src: "res/img/f_turret.png"},
	 {name: "w_turret",       type:"image", src: "res/img/w_turret.png"},
	 {name: "w_powerup",      type:"image", src: "res/img/w_powerup-new-sheet.gif"},
	 {name: "f_powerup",      type:"image", src: "res/img/f_powerup-new-sheet.gif"},
	 {name: "w_projectile",   type:"image", src: "res/img/w_projectile.png"},
	 {name: "f_projectile",   type:"image", src: "res/img/f_projectile.png"},
	 {name: "health",         type:"image", src: "res/img/pear.png"},
	 {name: "demon_sheet",    type:"image", src: "res/img/demon_sheet.gif"},
	 {name: "shield_sheet",   type:"image", src: "res/img/shield_sheet.png"},
	 {name: "bg",             type:"image", src: "res/img/bg.png"},
	 {name: "bg2",            type:"image", src: "res/img/bg2.png"},
	 {name: "level_end",      type:"image", src: "res/img/level_end.png"},

	 //menus
	 {name: "credits_bg",  type:"image", src: "res/img/credits_bg.png"},
	 {name: "game_end_bg", type:"image", src: "res/img/game_end_bg.png"},
	 {name: "title_bg",    type:"image", src: "res/img/title_bg.png"},
	 {name: "play",        type:"image", src: "res/img/play.png"},
	 {name: "credits",     type:"image", src: "res/img/credits.png"},
	 {name: "selection",   type:"image", src: "res/img/selection.png"},

		
	/* Maps. 
	 * @example
	 * {name: "example01", type: "tmx", src: "data/map/example01.tmx"},
 	 */
	 {name: "level_test", type: "tmx", src: "res/level_1_demo.tmx"},
	 {name: "level_1", type: "tmx", src: "res/level_1.tmx"},
	 {name: "level_2", type: "tmx", src: "res/level_2.tmx"},
	 {name: "level_3", type: "tmx", src: "res/level_3.tmx"},
	
	/* Sound effects. 
	 * @example
	 * {name: "example_sfx", type: "audio", src: "data/sfx/", channel : 2}
	 */
	 {name: "g_mode", type: "audio", src: "res/sfx/", channel : 1},
	 {name: "f_mode", type: "audio", src: "res/sfx/", channel : 1},
	 {name: "w_mode", type: "audio", src: "res/sfx/", channel : 1},
	 {name: "health", type: "audio", src: "res/sfx/", channel : 1},
	 {name: "powerup", type: "audio", src: "res/sfx/", channel : 1},
	 {name: "w_absorb", type: "audio", src: "res/sfx/", channel : 1},
	 {name: "f_absorb", type: "audio", src: "res/sfx/", channel : 1},
	 {name: "w_hit", type: "audio", src: "res/sfx/", channel : 1},
	 {name: "f_hit", type: "audio", src: "res/sfx/", channel : 1},
	 {name: "death", type: "audio", src: "res/sfx/", channel : 1},
	 {name: "level_start", type: "audio", src: "res/sfx/", channel : 1},
	 {name: "level_end", type: "audio", src: "res/sfx/", channel : 1},
	 {name: "jump", type: "audio", src: "res/sfx/", channel : 1}
];
