game.CreditsScreen = me.ScreenObject.extend(
{
	init: function()
	{ 
		this.parent(true, true);
		this.background = null; 
	}, 
	
	/**	
	 *  action to perform on state change
	 */
	onResetEvent: function() {	
			
            this.background = me.loader.getImage("credits_bg");;

	},
	
	update: function()
	{
		this.parent();
        if (me.input.isKeyPressed("back") || me.input.isKeyPressed("go"))
			me.state.change(me.state.TITLE);
		
		return true;
	},
	
	draw: function(context)
	{
        context.drawImage(this.background, 0,0, me.video.getWidth(), me.video.getHeight());
	}
});

game.GameEndScreen = me.ScreenObject.extend(
{
	init: function()
	{ 
		this.parent(true, true);
		this.background = null; 
	}, 
	
	/**	
	 *  action to perform on state change
	 */
	onResetEvent: function() {	
			
            this.background = me.loader.getImage("game_end_bg");;

	},
	
	update: function()
	{
		this.parent();
        if (me.input.isKeyPressed("back") || me.input.isKeyPressed("go"))
			me.state.change(me.state.TITLE);
		
		return true;
	},
	
	draw: function(context)
	{
        context.drawImage(this.background, 0,0, me.video.getWidth(), me.video.getHeight());
	}
});
