///////////////////////////////////////////////////////////
//  CONSTANTS
///////////////////////////////////////////////////////////

//mode enum
var MODE_TYPES = 
{
	NONE : 0,
	FIRE : 1,
	WATER: 2
};

//layers enum
var LAYERS = 
{
	PLAYER:     10,
	TURRET:     11,
	PROJECTILE: 12,
	PICKUP:     13,
	DEMON:      9
};

var CHANGE_COOLDOWN = 1; //kind of a relic, but needed for the code :(
var PROJECTILE_SPEED = 3;

///////////////////////////////////////////////////////////
//  PLAYER ENTITY
///////////////////////////////////////////////////////////
var PlayerEntity = me.ObjectEntity.extend(
{

	init:function (x, y, settings)
	{
		this.lastTime         = new Date().getTime();
		this.currentAnimation = "walk";
		this.mode             = MODE_TYPES.NONE;
		this.changeCooldown   = 0;
		this.startX = x;
		this.startY = y;
		
		// call the constructor
		this.parent(x, y , settings);
		
		// set the walking & jumping speed
		this.setVelocity(3.7, 14);
				
		// adjust the bounding box
		this.updateColRect(8, 16, 0, 32);
		
		// set the display to follow our position on both axis
		me.game.viewport.follow(this.pos, me.game.viewport.AXIS.BOTH);
		this.jumping = false;
		this.falling = false;
		this.damageCooldown = 0;
		this.renderable.alwaysUpdate = true;
				
//		alert(me.game.collisionMap.name);
		me.game.collisionMap = me.game.currentLevel.getLayerByName("collision_g");
		this.collisionMap = me.game.collisionMap;
		me.game.currentLevel.getLayerByName("f_f").setOpacity(.2);
		me.game.currentLevel.getLayerByName("f_w").setOpacity(.2);
//		alert(me.game.collisionMap.name);
		
		me.gamestat.setValue("wEnabled", settings.w_enabled);
		me.gamestat.setValue("fEnabled", settings.f_enabled);
		
		//demon buddy!!!
		this.xFlipped = false;
		this.demon = me.entityPool.newInstanceOf("demon", this);
//		me.game.add(this.demon, LAYERS.DEMON);
		
		//shield animations
		this.shield = me.entityPool.newInstanceOf("shield", this);
		this.shield.visible = false;
//		me.game.add(this.shield, LAYERS.PICKUP);
//		me.game.sort();

		me.audio.play("level_start");
		this.renderable.addAnimation("stand", [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22]);
		this.renderable.addAnimation("walk", [23,24,25,26,27,28]);
		this.renderable.setCurrentAnimation("stand", "stand");
//		this.addAnimation("soul", [8,9,10]);
//		this.addAnimation("soulIn", [11,,12,13]);
//		this.addAnimation("soulOut", [13,12,11]);
		
	},
	
	update : function ()
	{

		if (me.input.isKeyPressed('left') || me.input.isKeyPressed("left2")){
			this.doWalk(true);
			this.xFlipped = true;
			this.renderable.setCurrentAnimation("walk", "walk");
		}
		else if (me.input.isKeyPressed('right') || me.input.isKeyPressed("right2")){
			this.xFlipped = false;
			this.doWalk(false);
			this.renderable.setCurrentAnimation("walk", "walk");
		} else
		{
			this.vel.x = 0;
			this.renderable.setCurrentAnimation("stand", "stand");
		}

		if (me.input.isKeyPressed('jump') || me.input.isKeyPressed("jump2"))
		{
			if (!this.jumping && !this.falling)	
			{
				this.vel.y = -this.maxVel.y * me.timer.tick;
				this.jumping = true;
				me.audio.play("jump");
			}
		}
		
		if (this.changeCooldown <= 0)
		{
			if (me.gamestat.getItemValue("wEnabled") == 1 &&
					(me.input.isKeyPressed("wswitch") || me.input.isKeyPressed("wswitch2")))
			{
				this.mode = MODE_TYPES.WATER;
				me.audio.play("w_mode");
			} 
			else if (me.gamestat.getItemValue("fEnabled") == 1 &&
					(me.input.isKeyPressed("fswitch") || me.input.isKeyPressed("fswitch2")))
			{
				this.mode = MODE_TYPES.FIRE;
				me.audio.play("f_mode");
			}
			else if (me.input.isKeyPressed("gswitch") || me.input.isKeyPressed("gswitch2"))
			{
				this.mode = MODE_TYPES.NONE;
				me.audio.play("g_mode");
			}
				
				
			switch(this.mode)
			{
			case MODE_TYPES.FIRE:
				me.game.collisionMap = me.game.currentLevel.getLayerByName("collision_f");
				me.game.currentLevel.getLayerByName("f_f").setOpacity(1);
				me.game.currentLevel.getLayerByName("f_w").setOpacity(.2);
				break;
			case MODE_TYPES.WATER:
				me.game.collisionMap = me.game.currentLevel.getLayerByName("collision_w");
				me.game.currentLevel.getLayerByName("f_f").setOpacity(.2);
				me.game.currentLevel.getLayerByName("f_w").setOpacity(1);
				break;
			case MODE_TYPES.NONE:
				me.game.collisionMap = me.game.currentLevel.getLayerByName("collision_g");
				me.game.currentLevel.getLayerByName("f_f").setOpacity(.2);
				me.game.currentLevel.getLayerByName("f_w").setOpacity(.2);
				break;
			}
			this.collisionMap = me.game.collisionMap;
			this.changeCooldown = CHANGE_COOLDOWN;
		}

		this.changeCooldown -= this.changeCooldown == 0 ? 0 : 1; 
		
		// check & update player movement
		this.updateMovement();
		this.shield.update();
		this.demon.update();
		
		//fall of cliff
		if (this.pos.y >= me.game.currentLevel.rows * me.game.currentLevel.tileheight)
		{
			this.die();
		} else
		{
			this.parent(this);
		}		

		// check for collision
		var res = me.game.collide(this);
		
		if (res)
		{
			if (res.type == me.game.ENEMY_OBJECT && this.damageCooldown <= 0){

				//see if we're in the wrong mode
				if (this.mode != res.obj.elType)
				{
					// let's flicker in case we touched an enemy
					this.renderable.flicker(45);
					this.damageCooldown = 45;
					me.game.viewport.shake(10, 500, me.game.viewport.AXIS.BOTH);
			    	me.gamestat.setValue("health", me.gamestat.getItemValue("health") - 1);
			    	me.game.HUD.updateItemValue("score", me.gamestat.getItemValue("health"));
					if (res.obj.elType == MODE_TYPES.FIRE)
						me.audio.play("f_hit");
					else
						me.audio.play("w_hit");
						
					//check for death
					if (me.gamestat.getItemValue("health") == 0)
						this.die();
							
				} else
				{
					//show shield
					this.shield.setMode(this.mode);
					if (res.obj.elType == MODE_TYPES.FIRE)
						me.audio.play("f_absorb");
					else
						me.audio.play("w_absorb");
				}
			}
		}
		if (this.damageCooldown > 0)
		{ this.damageCooldown -= me.timer.tick; }

//		
		return true;
	
	},
	
	draw: function(context)
	{
		this.parent(context);
		this.demon.draw(context);
		if (this.shield.visible)
			this.shield.draw(context);
		return true;
	},
	
	die: function()
	{
		me.game.viewport.fadeOut("#000000", 1000, this.respawn());
		me.audio.play("death");
	},
	
	respawn: function()
	{
		this.pos.x = this.startX;
		this.pos.y = this.startY;
		this.shield.pos.x = this.pos.x;
		this.shield.pos.y = this.pos.y;
		this.demon.pos.x = this.pos.x;
		this.demon.pos.y = this.pos.y;
		me.gamestat.setValue("health", 5);
	}

});

///////////////////////////////////////////////////////////
//DEMON ENTITY
///////////////////////////////////////////////////////////
var DemonEntity = me.ObjectEntity.extend(
{
	setReady: function()
	{ this.isReady = true;  },
	
	init: function(player)
	{
		var settings = {}
		settings.spritewidth = 32;
		settings.spriteheight = 32;
		settings.image = "demon_sheet";

		this.parent(player.pos.x, player.pos.y, settings);
		this.gravity = 0;
		this.pos.x = player.pos.x;
		this.pos.y = player.pos.y;
		this.collidable = false;
		
		this.player = player;
		
		this.target  = new me.Vector2d(player.pos.x, player.pos.y); //the place where we want to be to follow the character
		this.tweener = new me.Tween(this.pos);

		this.mode = MODE_TYPES.NONE;
		this.pos.isReady = true;
		
		this.renderable.addAnimation("earth", [0,1,2,3]);
		this.renderable.addAnimation("fire",  [4,5,6,7]);
		this.renderable.addAnimation("water", [8,9,10,11]);
		this.renderable.setCurrentAnimation("earth", "earth");
	},
	
	update: function()
	{
		this.parent(this);

				
		//update demon based on player
		if (this.pos.isReady)
		{
			if (this.player.xFlipped)
			{
				this.setTarget(this.player.pos.x + 24, this.player.pos.y);
			} else
			{
				this.setTarget(this.player.pos.x - 24, this.player.pos.y);
			}	
			this.tweener.stop();
			this.tweener.to({x: this.target.x, y: this.target.y}, 250);
			this.tweener.onComplete(this.setReady);
			this.tweener.easing(me.Tween.Easing.Linear.EaseNone);
			this.tweener.start();
			this.pos.isReady = false;
		}
		

		//for now, go automatically to the target spot, should tween later
//		this.vel.y = this.target.y - this.pos.y;
//		this.vel.x = this.target.x - this.pos.x;

		//facing
		if (this.target.x - this.pos.x > 0)
		{ this.renderable.flipX(false); }
		else if (this.target.x - this.pos.x < 0)
		{ this.renderable.flipX(true); }
		else
		{
			this.renderable.flipX(this.player.xFlipped);
		}
		
		//mode
		this.setMode(this.player.mode);
//		this.updateMovement();
		return true;
	},
	
	setTarget: function(x, y)
	{
		this.target.x = x;
		this.target.y = y;
	},
	
	setMode: function(mode)
	{
		this.mode = mode;
		//update animation
		switch (mode)
		{
		case MODE_TYPES.NONE:
			this.renderable.setCurrentAnimation("earth", "earth");
			break;
		case MODE_TYPES.FIRE:
			this.renderable.setCurrentAnimation("fire", "fire");
			break;
		case MODE_TYPES.WATER:
			this.renderable.setCurrentAnimation("water", "water");
			break;
		}
	}
});

///////////////////////////////////////////////////////////
//SHIELD ENTITY
///////////////////////////////////////////////////////////
var ShieldEntity = me.ObjectEntity.extend(
{
	visibleCooldown: 0,
	init: function(player)
	{
		this.player = player;
		var settings = {}
		settings.spritewidth = 32;
		settings.spriteheight = 32;
		settings.image = "shield_sheet";
				
		this.parent(player.pos.x, player.pos.y, settings);
		this.pos.x = player.pos.x;
		this.pos.y = player.pos.y;
		this.renderable.alpha = .3;
		this.gravity = 0;
		this.collidable = false;
		this.renderable.addAnimation("fire",  [0,1,2]);
		this.renderable.addAnimation("water", [3,4,5]);
		this.renderable.setCurrentAnimation("fire");
		this.mode = MODE_TYPES.FIRE;
	},
	
	update: function()
	{
		this.parent(this);
		this.pos.x = this.player.pos.x;
		this.pos.y = this.player.pos.y;
		if (this.visibleCooldown > 0)
			this.visibleCooldown--;
			
		if (this.visibleCooldown == 0)
			this.visible = false;
		else
			this.visible = true;
		return true;
	},
	
	setMode: function(mode)
	{
		this.mode = mode;
		if (mode == MODE_TYPES.FIRE)
		{
			this.renderable.setCurrentAnimation("fire");
		} else
		{
			this.renderable.setCurrentAnimation("water");
		}
		this.renderable.setAnimationFrame();
		this.visibleCooldown = 45;
		this.visible = true;
	}
});

///////////////////////////////////////////////////////////
//PROJECTILE ENTITY
///////////////////////////////////////////////////////////
var ProjectileEntity = me.ObjectEntity.extend(
{
init: function(type, x, y, xvel, yvel)
{
	var settings = {}
	settings.spritewidth = 16;
	settings.spriteheight = 16;
	settings.type = me.game.ENEMY_OBJECT;
	if (type == MODE_TYPES.WATER)
		settings.image = "w_projectile";
	else
		settings.image = "f_projectile";
			
	this.parent(x, y, settings);
	this.gravity = 0;
	this.pos.x = x;
	this.pos.y = y;
	this.setVelocity(Math.abs(xvel), Math.abs(yvel));
	this.vel.x = xvel;
	this.vel.y = yvel;
	this.prevX = this.pos.x;
	this.prevY = this.pos.y;
	this.elType = type;
	if (this.vel.x < 0)
		this.flipX(true);
	if (this.vel.y < 0)
		this.flipY(true);
},

update: function()
{
	this.parent(this);
	//just update the movement and move along, the collision handler will you know... handle collisions
	this.prevX = this.pos.x;
	this.prevY = this.pos.y;
	this.updateMovement();
	if (!me.game.viewport.isVisible(this) ||
			this.prevX == this.pos.x && this.prevY == this.pos.y)
	{
		//kill off
		this.alive = false;
		me.game.remove(this);
	}
	return true;
},

onCollision: function(res, obj) 
{
    // remove it
	this.alive = false;
    me.game.remove(this);
}
});

///////////////////////////////////////////////////////////
//  TURRET ENTITY - SHOOTS PROJECTILES
///////////////////////////////////////////////////////////
var TurretEntity = me.ObjectEntity.extend(
{
	//members
	shootDirection: null,
	SHOT_COOLDOWN:  30, //max cooldown, will be read from map
	cooldown:       this.SHOT_COOLDOWN,
	type:           MODE_TYPES.NONE,

	//get the position and settings from tiled map
	init: function(x, y, settings)
	{
        if (settings.type.indexOf("f") != -1)
        	settings.image = "f_turret";
        else if (settings.type.indexOf("w") != -1)
        	settings.image = "w_turret";
        	
        settings.spritewidth = 16;
        settings.spriteheight = 32;
		this.parent(x, y, settings);
		this.gravity = 0;
		this.pos.x = x;
		this.pos.y = y;
		
		this.shootDirection = new me.Vector2d(0,0);
		this.shootDirection.x = settings.shootDirectionX;
		this.shootDirection.y = settings.shootDirectionY;
		this.shootDirection.normalize();
		this.shootDirection.x *= PROJECTILE_SPEED;
		this.shootDirection.y *= PROJECTILE_SPEED;
		
		this.SHOT_COOLDOWN = settings.cooldown;
		this.cooldown = this.SHOT_COOLDOWN;
		this.collidable = false;
		
		this.type = settings.type;
        if (settings.type.indexOf("f") != -1)
        	this.type = MODE_TYPES.FIRE;
        else if (settings.type.indexOf("w") != -1)
        	this.type = MODE_TYPES.WATER;
	},
	
	update: function()
	{
		//no movement, just fire projectiles at a given rate
		this.cooldown -= me.timer.tick;
		if (this.cooldown <= 0)
		{
			//shoot
//			var projectile = me.entityPool.newInstanceOf("projectile", this.type,
//					this.pos.x + (this.width * .5), this.pos.y  + (this.height* .5), 
//					this.shootDirection.x, this.shootDirection.y);
			var projectile = me.entityPool.newInstanceOf("projectile", this.type,
					this.pos.x, this.pos.y+5, 
					this.shootDirection.x, this.shootDirection.y);
			me.game.add(projectile, LAYERS.PROJECTILE);
			me.game.sort();
			this.cooldown = this.SHOT_COOLDOWN; //reset cooldown
		}
		this.parent(this);
		return true;
	}	
});

var PowerupEntity = me.CollectableEntity.extend(
{
	type: MODE_TYPES.NONE, //this is the type of powerup, same as player modes, should be set in map

	//probably don't need any additional initialization..
    init: function(x, y, settings) 
    {
        // call the parent constructor
        this.parent(x, y, settings);
        if (settings.type.indexOf("f") != -1)
        	this.elType = MODE_TYPES.FIRE;
    	else if (settings.type.indexOf("w") != -1)
    		this.elType = MODE_TYPES.WATER;
    		
    	this.type = me.game.COLLECTABLE_OBJECT;
    },
    
    //don't really need to update either...
    update: function()
    {
    	this.parent(this);
    	return true;
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
    onCollision: function() 
    {
        // do something when collected
//    	me.audio.play("pickup");
        // make sure it cannot be collected "again"
        this.collidable = false;
        // remove it
        if (this.elType == MODE_TYPES.FIRE)
        {
        	me.gamestat.setValue("fEnabled", 1);
        }       	
    	else if (this.type == MODE_TYPES.WATER)
        {
        	me.gamestat.setValue("wEnabled", 1);
        }
        me.audio.play("powerup");
        me.game.remove(this);
    }
 
}
);

var HealthEntity = me.CollectableEntity.extend(
{
		//probably don't need any additional initialization..
    init: function(x, y, settings) 
    {
        // call the parent constructor
    	settings.image = "health";
    	settings.spritewidth = 16;
    	settings.spriteheight = 16;
        this.parent(x, y, settings);
    },
    
    //don't really need to update either...
    update: function()
    {
    	this.parent(this);
    	return true;
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
    onCollision: function() 
    {
    	//add health
    	me.gamestat.setValue("health", me.gamestat.getItemValue("health") + 1);
    	me.game.HUD.updateItemValue("score", me.gamestat.getItemValue("health"));
        // do something when collected
    	me.audio.play("health");
        // make sure it cannot be collected "again"
        this.collidable = false;
        // remove it
        me.game.remove(this);
    }
 
}
);

var GameEndEntity = me.CollectableEntity.extend(
{
		//probably don't need any additional initialization..
    init: function(x, y, settings) 
    {
        // call the parent constructor
        this.parent(x, y, settings);
    },
    
    //don't really need to update either...
    update: function()
    {
    	this.parent(this);
    	return true;
    },
 
    // this function is called by the engine, when
    // an object is touched by something (here collected)
    onCollision: function() 
    {
    	me.audio.play("level_end");
    	me.state.change(me.state.GAME_END);
    }
 
}
);

/*-------------- 
HUUUUUUUUUUUUUUUUUUUUUUUUUD
--------------------- */
game.ScoreObject = me.HUD_Item.extend(
{
    init: function(x, y) {
        // call the parent constructor
        this.parent(x, y);
        // create a font
        this.sprite = new me.SpriteObject(0,0, me.loader.getImage("health"), 16, 16);
    },
 
    /* -----
 
    draw our score
 
    ------ */
    draw: function(context, x, y) {
    	for (var i = 0; i < me.gamestat.getItemValue("health"); i++)
    	{
    		this.sprite.pos.x = 20*i;
    		this.sprite.draw(context);
    	}
    },
    
    update: function()
    {
    	this.parent(this);
    	this.sprite.update();
    	return true;
    }
 
});
